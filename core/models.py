from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save  
from django.utils import timezone
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _
from django.core.mail import send_mail
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from .managers import UserManager


class Trip(models.Model):
    ''' This descriped a trip '''
    departure_time = models.TimeField(auto_now=False)
    arrival_time = models.TimeField(auto_now=False)
    route = models.ForeignKey('Route',related_name='route')

    def __str__(self):
        return str(str(self.route)+ ' departs at '+ str(self.departure_time))

class Route(models.Model):
    '''Route models describes a route model '''
    start_point=models.CharField(max_length=100)
    end_point=models.CharField(max_length=100)
    distance=models.DecimalField(max_digits=5,decimal_places=2)

    def __str__(self):
        return str(str(self.start_point)+' - '+str(self.end_point))
    
class SystemUser(models.Model):
    """Create the users of the system.Including customers, admins and super admin
    Their roles will be separated by authorization"""
    user=models.OneToOneField(User,on_delete=models.CASCADE)
    national_id=models.IntegerField(null=True,blank=True)
    phone_number=models.CharField(max_length=11,null=True,blank=True)
    country_code=models.CharField(max_length=5,null=True,blank=True)

    def __str_(self):
        return str(self.user)

class Passenger(models.Model):
    ''' Descripes a trip Passanger ''' 
    passanger_name = models.CharField(max_length=50)
    source = models.CharField(max_length=100)
    destination = models.CharField(max_length=100)
    departure_time=models.TimeField()
    arrival_time = models.TimeField()
    distance = models.DecimalField(decimal_places=3,max_digits=7)
    date = models.DateField()
    train_class = models.CharField(max_length=50)
    amount_paid = models.DecimalField(max_digits=7,decimal_places=2,default=0.0)
    ticket = models.OneToOneField('Ticket')

    def __str__(self):
      return  str(self.passanger_name)


class Ticket(models.Model):
    '''Describe a Ticket models '''
    ticket_signiture=models.CharField(max_length=50,unique=True)
    user = models.ForeignKey('SystemUser')
    
    def __str__(self):
        return str(self.ticket_signiture)

class TrainClass(models.Model):
    ''' Description a TrainClass data model'''
    name=models.CharField(max_length=50)
    capacity=models.IntegerField()
    
    def __str__(self):
        return str(self.name)

class Price(models.Model):
    '''Description a price data model'''
    train_class=models.ForeignKey('TrainClass',related_name='train_class')
    trip = models.ForeignKey('Trip',related_name='trip')   
    absolute_value=models.IntegerField()
    actual_value=models.DecimalField(max_digits=6,decimal_places=2,default=0.00)
    
    def save(self, *args, **kwargs):
        super(Price, self).save(*args, **kwargs) # Call the real save() method#  

    def __str__(self):
        return str(self.train_class)+ ' ' + str(self.trip)

class Offer(models.Model):
    """
    Describes all the offers available for a ticket price
    """
    EFFECTS = (
        ('N','Null'),
        ('H','Hike price'),
        ('D','Discount price'),
    )
    name = models.CharField(max_length=100)
    description = models.TextField()
    effect = models.CharField(max_length=1,choices=EFFECTS)
    value = models.DecimalField(max_digits=6,decimal_places=2)
    price = models.ManyToManyField('Price')

    def __str__(self):
        return str(self.name)
   

class Booking(models.Model):
    ''' Descride a store for all the remaining seat to a given trip'''
    date = models.DateField()
    trip = models.ForeignKey(Trip,related_name='Trip')
    train_class = models.ForeignKey(TrainClass)
    remaining_seats = models.IntegerField()

    def __str__(self):
        return str(self.remaining_seats)

class PaymentMethod(models.Model):
    ''' Descride a store to all the payments methods avilable '''
    name=models.CharField(max_length=100)

    def __str__(self):
        return str(self.name)

class Transations(models.Model):
    customer = models.ForeignKey(SystemUser)
    method = models.ForeignKey(PaymentMethod)
    transaction_signature=models.CharField(max_length=50,unique=True)
    transaction_amount= models.DecimalField(max_digits=7,decimal_places=2,default=0.0)



