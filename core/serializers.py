from rest_framework import serializers
from core.models import Trip, Route, Ticket, TrainClass, Price, Offer,\
             SystemUser, Booking,Passenger,PaymentMethod,Transations
from django.contrib.auth.models import User
from core.booking_data import BookData,BookedSeatsData,BookedData,PaymentMethodData,FinalBookingData

# from .relations import OfferPricesSerializer


class UserSerialize(serializers.ModelSerializer):

    """
    Serializes the user
        """
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name',
                  'last_name', 'password', 'email')

    def create(self, validated_data):
        """
        Creates a new User
        """
        user = User(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user

    def update(self, instance, validated_data):
        """Updates and returns a new `User` Instance"""
        instance.username = validated_data.get('username', instance.username)
        instance.firts_name = validated_data.get(
            'firts_name', instance.firts_name)
        instance.last_name = validated_data.get(
            'last_name', instance.last_name)
        instance.password = validated_data.get('password', instance.password)
        instance.email = validated_data.get('email', instance.email)
        instance.save()
        return insta

class SystemUserSerializer(serializers.HyperlinkedModelSerializer):
    """serilizes the SystemUser"""
    user = serializers.HyperlinkedRelatedField(view_name='user-detail', queryset=User.objects.all())
    
    class Meta:
        model = SystemUser
        fields = ('id','user','national_id','phone_number','country_code')

    def create(self, validated_data):
        """
        Create and return a new `Trip` instance, given the validated data.
        """
        return SystemUser.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Trip` instance, given the validated data.
        """
        instance.user = validated_data.get(
            'user', instance.user)
        instance.national_id = validated_data.get(
            'national_id', instance.national_id)
        instance.phone_number = validated_data.get('phone_number', instance.phone_number)

        instance.country_code = validated_data.get(
            'country_code', instance.country_code)
        instance.save()
        return instance


class TripSerializer(serializers.HyperlinkedModelSerializer):
    """
    serializes the Trip Model
    """
    route = serializers.HyperlinkedRelatedField(
        view_name='route-detail', queryset=Route.objects.all())

    class Meta:
        model = Trip
        fields = ('id', 'departure_time', 'arrival_time', 'route')

    def create(self, validated_data):
        """
        Create and return a new `Trip` instance, given the validated data.
        """
        return Trip.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Trip` instance, given the validated data.
        """
        instance.departure_time = validated_data.get(
            'departure_time', instance.departure_time)
        instance.arrival_time = validated_data.get(
            'arrival_time', instance.arrival_time)
        instance.route = validated_data.get('route', instance.route)
        instance.save()
        return instance


class RouteSerializer(serializers.HyperlinkedModelSerializer):
    """serializes the Route Model"""
    class Meta:
        model = Route
        fields = ('id', 'start_point', 'end_point', 'distance')

    def create(self, validated_data):
        """
        Create and return a new `Route` instance, given the validated data.
        """
        return Route.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Route` instance, given the validated data.
        """
        instance.start_point = validated_data.get(
            'start_point', instance.start_point)
        instance.end_point = validated_data.get(
            'end_point', instance.end_point)
        instance.distance = validated_data.get('distance', instance.distance)
        instance.save()
        return instance


class TicketSerializer(serializers.HyperlinkedModelSerializer):
    """Serializes the Ticket model"""
    user = serializers.HyperlinkedRelatedField(view_name='sysuser-detail', queryset=SystemUser.objects.all())

    class Meta:
        model = Ticket

        fields = ('id', 'ticket_signiture', 'user')

    def create(self, validated_data):
        """
        Create and return a new `Ticket` instance, given the validated data.
        """
        return Ticket.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Ticket` instance, given the validated data.
        """
        instance.ticket_signiture = validated_data.get(
            'ticket_signiture', instance.ticket_signiture)
        instance.user = validated_data.get('user', instance.user)
        instance.save()
        return instance


class TrainClassSerializer(serializers.HyperlinkedModelSerializer):
    """Serializes the trainclass"""

    class Meta:
        model = TrainClass
        fields = ('id', 'name',
                  'capacity')

    def create(self, validated_data):
        """
        Create and return a new `Train` instance, given the validated data.
        """
        return TrainClass.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Route` instance, given the validated data.
        """
        instance.name = validated_data.get('name', instance.name)
        instance.capacity = validated_data.get('capacity', instance.capacity)
        instance.save()
        return instance


class OfferSerializer(serializers.HyperlinkedModelSerializer):
    """Serializes the offers model"""
    price = serializers.HyperlinkedRelatedField(view_name='price_detail',queryset=Price.objects.all(),many=True)
    class Meta:
        model = Offer
        fields = ('id', 'name', 'description', 'effect', 'value','price')

    def create(self, validated_data):
        """
        Create and return a new `Offer` instance, given the validated data.
        """
        return Offer.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Offer` instance, given the validated data.
        """
        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get(
            'description', instance.description)
        instance.effect = validated_data.get('effect', instance.effect)
        instance.value = validated_data.get(
            'value', instance.value)
        instance.price = validated_data.get('price', instance.price)
        instance.save()
        return instance


class PriceSerializer(serializers.HyperlinkedModelSerializer):
    """Serializes the price model"""
    train_class = serializers.HyperlinkedRelatedField(
        view_name='trainclass-detail', queryset=TrainClass.objects.all()
    )
    trip = serializers.HyperlinkedRelatedField(
        view_name='trip-detail', queryset=Trip.objects.all()
    )

    class Meta:
        model = Price
        fields = ('id', 'train_class', 'absolute_value',
                  'actual_value', 'trip')

    def create(self, validated_data):
        """
        Create and return a new `Train` instance, given the validated data.
        """
        return Price.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Route` instance, given the validated data.
        """
        instance.absolute_value = validated_data.get(
            'absolute_value', instance.absolute_value)
        instance.actual_value = validated_data.get(
            'actual_value', instance.actual_value)
        instance.train_class = validated_data.get(
            'train_class', instance.train_class)
        instance.trip = validated_data.get('trip', instance.trip)
        instance.save()
        return instance


class BookingSerializer(serializers.HyperlinkedModelSerializer):
    """serilizes the booking model"""
    trip = serializers.HyperlinkedRelatedField(
        view_name='trip-detail', queryset=Trip.objects.all())
    train_class = serializers.HyperlinkedRelatedField(
        view_name='trainclass-detail', queryset=TrainClass.objects.all())

    class Meta:
        model = Booking
        fields = ('id', 'date', 'trip', 'train_class', 'remaining_seats')

    def create(self, validated_data):
        """
        Create and return a new `Train` instance, given the validated data.
        """
        return Booking.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Route` instance, given the validated data.
        """
        instance.date = validated_data.get('date', instance.date)
        instance.trip = validated_data.get('trip', instance.trip)
        instance.remaining_seats = validated_data.get(
            'remaining_seats', instance.remaining_seats)
        instance.train_class = validated_data.get(
            'train_class', instance.train_class)
        instance.save()
        return instance

class PassengerSerializer(serializers.HyperlinkedModelSerializer):
    """Serializes the passangers model"""

    class Meta:
        model = Passenger
        fields = ('id','passanger_name','source','destination','departure_time','arrival_time','distance','date','train_class','amount_paid')

    def create(self, validated_data):
        """
        Create and return a new `Train` instance, given the validated data.
        """
        return Passenger.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Route` instance, given the validated data.
        """
        instance.date = validated_data.get('date', instance.date)
        instance.source = validated_data.get('source', instance.source)
        instance.destination = validated_data.get(
            'destination', instance.destination)
        instance.departure_time = validated_data.get(
            'departure_time', instance.departure_time)
        instance.arrival_time = validated_data.get(
            'arrival_time', instance.arrival_time)
        instance.distance = validated_data.get(
            'distance', instance.distance)
        instance.train_class = validated_data.get(
            'train_class', instance.train_class)
        instance.amount_paid = validated_data.get(
            'amount_paid', instance.amount_paid)
        instance.save()
        return instance

class PaymentMethodSerializer(serializers.HyperlinkedModelSerializer):
    """serializes the paymentMethod model"""
    class Meta:
        model = PaymentMethod
        fields = ('id','name')

    def create(self, validated_data):
        """
        Create and return a new `PaymentMethod` instance, given the validated data.
        """
        return PaymentMethod.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `PaymentMethod` instance, given the validated data.
        """
        instance.name = validated_data.get(
            'name', instance.name)
        instance.save()
        return instance

class TransactionsSerializer(serializers.HyperlinkedModelSerializer):
    """Serializes the `Transations` model"""
    customer = serializers.HyperlinkedRelatedField(view_name='sysuser-detail',queryset=SystemUser.objects.all())
    method = serializers.HyperlinkedRelatedField(view_name='paymentmethod-detail',queryset=PaymentMethod.objects.all())
    class Meta:
        model = Transations
        fields = ('id','customer','method','transaction_signature')

    def create(self, validated_data):
        """
        Create and return a new `Train` instance, given the validated data.
        """
        return Transations.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Route` instance, given the validated data.
        """
        instance.customer = validated_data.get('customer', instance.customer)
        instance.method = validated_data.get(
            'method', instance.method)
        instance.transaction_signature = validated_data.get(
            'transaction_signature', instance.transaction_signature)
        instance.save()
        return instance

class BookingDataSerializer(serializers.Serializer):
    trip_id = serializers.IntegerField()
    train_class_id = serializers.IntegerField()
    date = serializers.DateField()

    def create(self,validated_data):
        return BookData(**validated_data)


class BookedDataSerializer(serializers.Serializer):
    date = serializers.DateField()
    no_of_seats = serializers.IntegerField()
    total_price = serializers.DecimalField(max_digits=6, decimal_places=2)
    payment_url = serializers.CharField()

    def create(self,validated_data):
        return BookedData(**validated_data)

class BookedSeatsSerializer(serializers.Serializer):
    trip_id = serializers.IntegerField()
    train_class_id = serializers.IntegerField()
    date = serializers.DateField()
    no_of_seats = serializers.IntegerField()

    def create(self,validated_data):
        return BookedSeatsData(**validated_data)

class PaymentDataSerializer(serializers.Serializer):
    method_id = serializers.IntegerField()

    def create(self,validated_data):
        return PaymentMethodData(**validated_data)

class FinalBookingDataSerializer(serializers.Serializer):
    """Serializes the final booking data before the generation of the `Ticket`
    to the `User`
    """
    user = serializers.CharField()
    trip_id = serializers.IntegerField()
    train_class_id = serializers.IntegerField()
    date = serializers.DateField()
    no_of_seats = serializers.IntegerField()
    phone_number = serializers.CharField()

    def create(self,validated_data):
        return FinalBookingData(**validated_data)
