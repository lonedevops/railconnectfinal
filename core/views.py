from core.serializers import TripSerializer, RouteSerializer,\
    TicketSerializer,\
    TrainClassSerializer, PriceSerializer, OfferSerializer, UserSerialize,\
    BookingSerializer,SystemUserSerializer,PassengerSerializer,BookingDataSerializer,\
    BookedSeatsSerializer,BookedDataSerializer,PaymentDataSerializer,TransactionsSerializer,\
    PaymentMethodSerializer,FinalBookingDataSerializer
from .booking_data  import BookedData
from core.models import Trip, Route,Ticket, TrainClass, Price, Offer,\
                Booking,SystemUser,Passenger,PaymentMethod,Transations
from django.contrib.auth.models import User
from django.http import Http404
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.request import Request
from rest_framework import generics
from django.utils.six import BytesIO
from rest_framework.parsers import JSONParser
from django.db import transaction
from .booking import generate_a_ticket_signature

from . payments import generate_a_transaction_key
@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'trips': reverse('trip-list', request=request, format=format),
        'routes': reverse('route-list', request=request, format=format),
        'tickets': reverse('ticket-list', request=request, format=format),
        'prices': reverse('price-list', request=request, format=format),
        'trainclasses': reverse('trainclass-list', request=request, format=format),
        'obtain-token': reverse('token-auth', request=request, format=format),
        'register': reverse('register', request=request, format=format),
        'offers': reverse('offers-list', request=request,format=format),
        'bookings': reverse('booking-list', request=request, format=format),
        'sysusers': reverse('sysuser-list', request=request, format=format),
        'passangers': reverse('passanger-list', request=request,format=format),
        'book-step1': reverse('booking-step1', request=request, format=format),
        'booking-step2': reverse('booking-step2', request=request,format=format),
        'booking-step3': reverse('booking-step3', request=request,format=format),
        'booking-step4': reverse('booking-step4', request=request,format=format),
        'booking-step5': reverse('booking-step5', request=request,format=format),
        'paymentmethods': reverse('paymentmethod-list', request=request,format=format),
        'transations': reverse('transaction-list', request=request,format=format),
        
    })


class UserList(generics.ListCreateAPIView):
    """Lists each user in the system"""
    queryset = User.objects.all()
    serializer_class = UserSerialize


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerialize


class TripList(generics.ListCreateAPIView):
    """
    List all trips that are avalibale, or create a new trip.
    """
    queryset = Trip.objects.all()
    serializer_class = TripSerializer


class TripDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, Update or delete a Trip instance
    """
    queryset = Trip.objects.all()
    serializer_class = TripSerializer


class RouteList(generics.ListCreateAPIView):
    """
    List all Routes that are avalibale, or create a new Route.
    """
    queryset = Route.objects.all()
    serializer_class = RouteSerializer


class RouteDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, Update or delete a Route instance
    """
    queryset = Route.objects.all()
    serializer_class = RouteSerializer


class TicketList(generics.ListCreateAPIView):
    """
    List all TicketList that are avalibale, or create a new TicketList.
    """
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer


class TicketDetail(generics.RetrieveUpdateDestroyAPIView):

    """
    Retrieve, Update or delete a Ticket instance
    """
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer


class TrainClassList(generics.ListCreateAPIView):
    """
    List all trainclass that are avalibale, or create a new trainclass.
    """
    queryset = TrainClass.objects.all()
    serializer_class = TrainClassSerializer

class TrainClassDetail(generics.RetrieveUpdateDestroyAPIView):

    """
    Retrieve, Update or delete a TrainClass instance
    """
    queryset = TrainClass.objects.all()
    serializer_class = TrainClassSerializer

class PriceList(generics.ListCreateAPIView):
    """
    List all PriceList that are avalibale, or create a new PriceList.
    """
    queryset = Price.objects.all()
    serializer_class = PriceSerializer

class PriceDetail(generics.RetrieveUpdateDestroyAPIView):

    """
    Retrieve, Update or delete a Price instance
    """
    queryset = Price.objects.all()
    serializer_class = PriceSerializer


class OfferList(generics.ListCreateAPIView):
    """
    Lists all the offers
    """
    queryset = Offer.objects.all()
    serializer_class = OfferSerializer

class OfferDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Lists a specific Offer
    """
    queryset = Offer.objects.all()
    serializer_class = OfferSerializer

class BookingList(generics.ListCreateAPIView):
    """
    List all the bookings
    """
    queryset = Booking.objects.all()
    serializer_class = BookingSerializer

class BookingDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Fetch a booking
    """

    queryset = Booking.objects.all()
    serializer_class = BookingSerializer

class SystemUserList(generics.ListCreateAPIView):
    """
    Lists the system Users
    """
    queryset = SystemUser.objects.all()
    serializer_class = SystemUserSerializer

class SystemUserDetail(generics.RetrieveUpdateDestroyAPIView):
    """Fetch each sysuser"""
    queryset = SystemUser.objects.all()
    serializer_class = SystemUserSerializer

class PassengerList(generics.ListCreateAPIView):
    queryset = Passenger.objects.all()
    serializer_class = PassengerSerializer

class PassangerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Passenger.objects.all()
    serializer_class = PassengerSerializer

class PaymentMethodList(generics.ListCreateAPIView):
    queryset = PaymentMethod.objects.all()
    serializer_class = PaymentMethodSerializer

class PaymentMethodDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = PaymentMethod.objects.all()
    serializer_class = PaymentMethodSerializer

class TranasctionList(generics.ListCreateAPIView):
    queryset = Transations.objects.all()
    serializer_class = TransactionsSerializer

class TransactionDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Transations.objects.all()
    serializer_class = TransactionsSerializer


@api_view(['GET'])
def Booking_List(request,format=None):
    """
    Sends all the trips available and trainclasses to anyone trying to book
    """
    if request.method == 'GET':
        trips = Trip.objects.all()
        tripserializer = TripSerializer(trips,many=True,context={'request': request})
        trainclasses = TrainClass.objects.all()
        trainclassserializer = TrainClassSerializer(trainclasses,many=True)
        return Response(data={"trips":tripserializer.data,"train_class":trainclassserializer.data})
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def BookingData(request,format=None):
    """
    Gets data sent by the user,`trip_id`,`train_class_id`,`date` which is used
    to check for remaining seats so the user can continue the booking process
    """
    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = BookingDataSerializer(data=data)
        if serializer.is_valid():
            book_data = serializer.save()
            try:
                booking = Booking.objects.get(date=book_data.date,trip__id=book_data.trip_id,train_class__id=book_data.train_class_id)
            except Booking.DoesNotExist:
                 train_class = TrainClass.objects.get(pk=book_data.train_class_id)
                 trip = Trip.objects.get(pk=book_data.trip_id)
                 booking = Booking(date=book_data.date,trip=trip,train_class=train_class,remaining_seats=train_class.capacity)
                 booking.save()
                 booking_serializer = BookingSerializer(booking,context={'request': request})
                 return Response(booking_serializer.data,status=status.HTTP_200_OK)
            booking_serializer = BookingSerializer(booking,context={'request': request})
            return Response(booking_serializer.data,status=status.HTTP_200_OK)
    return Response(serializer.errors,status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@transaction.atomic
def BookedSeats(request,format=None):
    """
    It post to the server the number of seats the user wants to book , substracts it from the 
    remaining seats and then return the total price for the seats if available.
    Also the user is supposed to choose his/her payment method
    """
    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer=BookedSeatsSerializer(data=data)
        if serializer.is_valid():
            booked_seat_data = serializer.save()
            booking = Booking.objects.get(date=booked_seat_data.date,trip__id=booked_seat_data.trip_id,train_class__id=booked_seat_data.train_class_id)
            new_seats =booking.remaining_seats - booked_seat_data.no_of_seats
            booking.remaining_seats = new_seats
            booking.save()
            train_class = TrainClass.objects.get(pk=booked_seat_data.train_class_id)
            trip = Trip.objects.get(pk=booked_seat_data.trip_id)
            train_class_serializer =TrainClassSerializer(train_class)
            trip_serializer = TripSerializer(trip,context={'request': request})
            price = Price.objects.get(trip__id=booked_seat_data.trip_id,train_class=booked_seat_data.train_class_id)
            total_price = price.actual_value * booked_seat_data.no_of_seats
            booked_data = BookedData(date=booked_seat_data.date,no_of_seats=booked_seat_data.no_of_seats,total_price=total_price,payment_url='http://localhost:8000/api/booking/step1/')
            book_data_serialized = BookedDataSerializer(booked_data) 
            return Response(data={"train_class":train_class_serializer.data,"trip":trip_serializer.data,"booking details":book_data_serialized.data},status=status.HTTP_200_OK)
    return Response(serializer.errors,status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def payment_method(request,format=None):
    '''Picks the payment method the user choose,verifies if it exists and then return status ok. '''
    if request.method == 'POST':
        data =JSONParser().parse(request)
        serializer = PaymentDataSerializer(data=data)
        if serializer.is_valid():
            payment_data = serializer.save()
            try:
                method =PaymentMethod.objects.get(pk=payment_data.method_id)
            except PaymentMethod.DoesNotExist:
                return Response(status = status.HTTP_404_NOT_FOUND)
            return Response(status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@transaction.atomic
def final(request,format=None):
    ''' Provide all the detals required to book a
    ticket and push all the changes to the database
    ,it check for the payment method id,trip_id,
    trip_class_id,user,date,no_of_seat_to_book
    and the phone number'''

    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer =FinalBookingDataSerializer(data=data)
        if serializer.is_valid():
            final_data = serializer.save()
            trip = Trip.objects.get(pk=final_data.trip_id)
            systemuser = SystemUser.objects.get(user__username = final_data.user)
            this_train_class = TrainClass.objects.get(pk=final_data.train_class_id)
            passager_name =str(systemuser.user.first_name)+' '+str(systemuser.user.last_name)
            source = trip.route.start_point
            destination =trip.route.end_point
            distance = trip.route.distance
            departure_time = trip.departure_time
            arrival_time=trip.arrival_time
            date=final_data.date
            train_class=this_train_class.name

            # function to generate ticket
            ticket_signiture = generate_a_ticket_signature(systemuser.user.first_name,date,train_class,distance)
            # create transaction key/id
            transaction_signature=generate_a_transaction_key(systemuser.user.first_name,date,train_class,distance)
            # save passabgerG
            # save ticket
            # save transaction
            price = Price.objects.get(trip__id=final_data.trip_id,train_class=final_data.train_class_id)
            total_price = price.actual_value * final_data.no_of_seats


            ticket = Ticket(ticket_signiture=ticket_signiture,user=systemuser)
            ticket.save()

            ticket_queried = Ticket.objects.get(ticket_signiture=ticket_signiture)
            print(ticket_queried)
            passanger = Passenger(passanger_name=passager_name,source=source,destination=destination,
                                    departure_time=departure_time,arrival_time=arrival_time,distance=distance,
                                    date=date,train_class=train_class,amount_paid=total_price,ticket=ticket_queried)
            passanger.save()
            passanger_last_saved = Passenger.objects.get(ticket=ticket_queried)
            #ticket = Ticket(ticket_signiture='qwerty123',passenger=passanger_last_saved)
           # ticket.save()
            method = PaymentMethod.objects.get(name="mpesa")
            # code to pay
            transaction = Transations(customer=systemuser,method=method,transaction_signature=transaction_signature,
                        transaction_amount=total_price)
            transaction.save()

            #ticket_saved = Ticket.objects.get(user=systemuser)
            ticket_seriliazed = TicketSerializer(ticket_queried,context={'request': request})
            passanger_Seriliazed = PassengerSerializer(passanger_last_saved)
            return Response(data={"ticket":ticket_seriliazed.data,"passanger_detail":passanger_Seriliazed.data},status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)

        





