from django import forms
from .models import Price


class OfferForm(forms.ModelForm):
    """Puts the price in a nice widget for the admin"""
    prices = forms.ModelMultipleChoiceField(queryset=Price.objects.all())