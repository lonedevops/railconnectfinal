from django.conf.urls import url
from core import views
from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
import djoser.views

router = routers.DefaultRouter()

urlpatterns = format_suffix_patterns([
    url(r'^$',  views.api_root),
    url(r'^users/$', views.UserList.as_view(),  name='user-list'),
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view(),name='user-detail'),
    url(r'^sysusers/$', views.SystemUserList.as_view(),  name='sysuser-list'),
    url(r'^sysusers/(?P<pk>[0-9]+)/$', views.SystemUserDetail.as_view(),name='sysuser-detail'),
    url(r'^trips/$', views.TripList.as_view(),  name='trip-list'),
    url(r'^trips/(?P<pk>[0-9]+)/$', views.TripDetail.as_view(),name='trip-detail'),
    url(r'^routes/$', views.RouteList.as_view(),  name='route-list'),
    url(r'^routes/(?P<pk>[0-9]+)/$',
        views.RouteDetail.as_view(), name='route-detail'),
    url(r'^tickets/$', views.TicketList.as_view(),  name='ticket-list'),
    url(r'^tickets/(?P<pk>[0-9]+)/$',
        views.TicketDetail.as_view(), name='ticket_detail'),
    url(r'^prices/$', views.PriceList.as_view(),  name='price-list'),
    url(r'^prices/(?P<pk>[0-9]+)/$',
        views.PriceDetail.as_view(), name='price_detail'),
    url(r'^trainclasses/$', views.TrainClassList.as_view(),  name='trainclass-list'),
    url(r'^trainclasses/(?P<pk>[0-9]+)/$',
        views.TrainClassDetail.as_view(), name='trainclass-detail'),
    url(r'^offers/$', views.OfferList.as_view(),  name='offers-list'),
    url(r'^bookings/$', views.BookingList.as_view(),  name='booking-list'),
    url(r'^bookings/(?P<pk>[0-9]+)/$', views.BookingDetail.as_view(),name='booking-detail'),
    url(r'^passangers/$', views.PassengerList.as_view(),  name='passanger-list'),
    url(r'^passangers/(?P<pk>[0-9]+)/$', views.PassangerDetail.as_view(),name='passanger-detail'),
    url(r'^paymentmethods/$', views.PaymentMethodList.as_view(),  name='paymentmethod-list'),
    url(r'^paymentmethods/(?P<pk>[0-9]+)/$', views.PaymentMethodDetail.as_view(),name='paymentmethod-detail'),
    url(r'^transactions/$', views.TranasctionList.as_view(),  name='transaction-list'),
    url(r'^transactions/(?P<pk>[0-9]+)/$', views.TransactionDetail.as_view(),name='transaction-detail'),
    url(r'^offers/(?P<pk>[0-9]+)/$', views.OfferDetail.as_view(),name='offer-detail'),
    url(r'^api-token-auth/', obtain_jwt_token, name='token-auth'),
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^api-token-verify/', verify_jwt_token),
    url(r'^auth/register', djoser.views.RegistrationView.as_view(), name='register'),
    url(r'^auth/password/reset', djoser.views.PasswordResetView.as_view()),
    url(r'^auth/password/confirm', djoser.views.PasswordResetConfirmView.as_view()),
    url(r'^me', djoser.views.UserView.as_view()),
    url(r'^booking/step1/', views.Booking_List,name='booking-step1'),
    url(r'^booking/step2/', views.BookingData,name='booking-step2'),
    url(r'^booking/step3/', views.BookedSeats,name='booking-step3'),
    url(r'^booking/step4/', views.payment_method,name='booking-step4'),
    url(r'^booking/step5/', views.final,name='booking-step5')

])

urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework'))
]
