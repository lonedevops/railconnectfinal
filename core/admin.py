from django.contrib import admin
from core.models import Trip,Route,Ticket,TrainClass,Price,Offer,SystemUser,Booking,Passenger,Transations,PaymentMethod
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .forms import OfferForm
# Classes to customize the admin interface

class TripAdmin(admin.ModelAdmin):
    fields = ('departure_time', 'arrival_time', 'route')
    list_display=('departure_time', 'arrival_time', 'route')


class SystemUserAdmin(admin.ModelAdmin):
    fields =('user','national_id','phone_number','country_code')
    list_display = ('user','national_id','phone_number','country_code')
    
class PassengerAdmin(admin.ModelAdmin):
    fields = ('passanger_name','source','destination','departure_time','arrival_time','distance','date','train_class','amount_paid')
    list_display = ('passanger_name','source','destination','departure_time','arrival_time','distance','date','train_class','amount_paid')

class RouteAdmin(admin.ModelAdmin):
    fields = ('start_point', 'end_point', 'distance')
    list_display=('start_point', 'end_point', 'distance')

class BookingAdmin(admin.ModelAdmin):
    fields = ('date','trip','train_class','remaining_seats')
    list_display = ('date','trip','train_class','remaining_seats')

class TicketAdmin(admin.ModelAdmin):
    fields = ('ticket_signiture', 'passenger')
    list_display=('ticket_signiture', 'passenger')

class TrainClassAdmin(admin.ModelAdmin):
    fields = ('name','capacity')
    list_display= ('name','capacity')

@admin.register(Price)
class PriceAdmin(admin.ModelAdmin):
    fields = ('train_class','trip','absolute_value')
    list_display=('absolute_value','actual_value','trip','train_class',)

class PriceInline(admin.TabularInline):
    model = Offer.price.through
    extra = 3

@admin.register(Offer)
class OfferAdmin(admin.ModelAdmin):
    inlines = [
        PriceInline,
    ]
    fields = ('name','description','effect','value')
    list_display = ('name', 'description','effect','value')

@admin.register(PaymentMethod)
class PaymentMethodAdmin(admin.ModelAdmin):
    fields = ('name',)
    list_display = ('name',)

@admin.register(Transations)
class TransationsAdmin(admin.ModelAdmin):
    fields =('method', 'customer','transaction_signature',)

    list_display= ('method','customer','transaction_signature',)

admin.site.register(Trip,TripAdmin)
admin.site.register(SystemUser,SystemUserAdmin)
admin.site.register(Passenger,PassengerAdmin)
admin.site.register(Route,RouteAdmin)
admin.site.register(Ticket,TicketAdmin)
admin.site.register(Booking,BookingAdmin)
admin.site.register(TrainClass,TrainClassAdmin)
