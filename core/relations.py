from .models import Offer,Price,OfferPrices
from rest_framework import serializers


class OfferPricesSerializer(serializers.HyperlinkedModelSerializer):
    """Serializing the many to many table"""
    
    prices = serializers.PrimaryKeyRelatedField(queryset=Price.objects.all())
    
    class Meta:
        model = OfferPrices
        fields = ('offers','prices')