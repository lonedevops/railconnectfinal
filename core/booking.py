import datetime
import random

def generate_a_ticket_signature(name,date,trainclass,distance):
    '''Generate a unique ticket_signature '''
    current_time=datetime.datetime.now().time()
    sqstring = str(name)+str(date)+str(trainclass)+str(distance)+str(current_time)
    sqlist =list(sqstring)
    print(sqlist)
    random.shuffle(sqlist)
    sqstring=str(sqlist).replace(':','').replace('\'','').replace(',','').replace(' ','').replace('.','').\
            replace('[','').replace(']','')
    return sqstring

