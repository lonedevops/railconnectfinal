from datetime import datetime

class BookData(object):
    """This class describes the booking data sent to data sent to the server for
    serialization"""
    def __init__(self,trip_id,train_class_id,date):
        self.trip_id = trip_id
        self.train_class_id = train_class_id
        self.date = date


    
class BookedSeatsData(object):
    """This class describes the booking data sent to data sent to the server for
    serialization"""
    def __init__(self,trip_id,train_class_id,date,no_of_seats):
        self.trip_id = trip_id
        self.train_class_id = train_class_id
        self.date = date
        self.no_of_seats = no_of_seats


class BookedData(object):
    """This class describes the booking data sent to data sent to the server for
    serialization"""
    def __init__(self,date,no_of_seats,total_price,payment_url):
        self.date = date
        self.no_of_seats = no_of_seats
        self.total_price = total_price
        self.payment_url = payment_url

class PaymentMethodData(object):
    """The payment class for serialization"""
    def __init__(self,method_id):
        self.method_id = method_id

class FinalBookingData(object):
    """all the data to complete the booking process"""
    def __init__(self, user,trip_id,train_class_id,date,no_of_seats,phone_number):
        self.user = user
        self.trip_id = trip_id
        self.train_class_id = train_class_id
        self.date = date
        self.no_of_seats = no_of_seats
        self.phone_number = phone_number
    
