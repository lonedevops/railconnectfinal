#!/bin/bash

#Run this script on the root of the project and the virtualenvironment must be active

python manage.py loadtestdata core.Trip:20
python manage.py loadtestdata core.Route:20
python manage.py loadtestdata core.Customer:20
python manage.py loadtestdata core.Train:20
python manage.py loadtestdata core.Ticket:20
python manage.py loadtestdata core.TrainClass:20
python manage.py loadtestdata core.Price:20