from django.conf.urls import url,include
from django.contrib import admin
from adminfuncs import views
from django.contrib.auth import views as login_views
from adminfuncs.forms import LoginForm
urlpatterns = [
    url(r'^$',views.index),
    url(r'^backend/refund/$',views.refundticket,name='refund'),
    url(r'^backend/(?P<username>\w+)/$',views.dashboard,name="dashboard"),
    url(r'^backend/(?P<username>\w+)/trips/$',views.admintrips,name="trips"),
    url(r'^backend/(?P<username>\w+)/trains/$',views.admintrips,name="trains"),
    url(r'^backend/(?P<username>\w+)/customers/$',views.admintrips,name="customers"),
    url(r'^backend/(?P<username>\w+)/tickets/$',views.admintrips,name="tickets"),
    url(r'^auth/logout/$', login_views.logout, {'next_page': '/'}),  
    url(r'^auth/login/$', login_views.login, {'template_name': 'adminfuncs/login.html', 'authentication_form': LoginForm},name='login'),

]