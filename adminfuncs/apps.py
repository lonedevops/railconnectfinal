from django.apps import AppConfig


class AdminfuncsConfig(AppConfig):
    name = 'adminfuncs'
