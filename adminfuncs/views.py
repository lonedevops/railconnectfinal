from django.shortcuts import render,redirect,get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from adminfuncs.forms import TripForm,RefundForm
from core.models import Trip,Route,Ticket,TrainClass,Price
from django.forms import modelformset_factory

def index(request):
    if request.user.is_authenticated:
        return redirect("dashboard",username=request.user)
    return redirect("login")

@login_required()
def dashboard(request,username):
     user = get_object_or_404(User, username=username)
     return render(request,"adminfuncs/index.html",{}) 

@login_required()
def admintrips(request,username):
    TripsFormSet = modelformset_factory(Trip, fields=('departure_time','arrival_time','route'))
    if request == 'POST':
        # handle update
        formset = AuthorFormSet(request.POST, request.FILES)
        if formset.is_valid():
            formset.save()
        return redirect(request,"adminfuncs/trips.html")
    else:
        formset = TripsFormSet()
    return render(request, 'adminfuncs/trips.html', {'formset': formset})
    # tripobject = Trip.objects.all()
    # return render(request,"adminfuncs/trips.html",{tripobject:tripobject,form:form})

@login_required()
def admintrains(request,username):
    return render(request,"adminfuncs/trains.html")

@login_required()
def adminroutes(request):
    return render(request,"adminfuncs/routes.html")

@login_required()
def admincustomers(request):
    return render(request,"adminfuncs/customers.html")

@login_required()
def admintrainclasses(request):
    return render(request,"adminfuncs/trainclasses.html")

@login_required()
def adminprices(request):
    return render(request,"adminfuncs/prices.html")


@login_required()
def refundticket(request):
    form = RefundForm()
    return render(request,"adminfuncs/refund.html",{"form":form})
