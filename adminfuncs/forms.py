from django.contrib.auth.forms import AuthenticationForm 
from django import forms
from django.forms import ModelForm
from core.models import Trip
# If you don't do this you cannot use Bootstrap CSS
class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=30, 
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
    password = forms.CharField(label="Password", max_length=30, 
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'password'}))
                    
class TripForm(ModelForm):
    class Meta:
        model=Trip
        fields = ['departure_time', 'arrival_time', 'route']
    
class RefundForm(forms.Form):
    name=forms.CharField(label='Enter Name of the customer', max_length=100,
    widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
